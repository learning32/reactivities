import React, { useState, FormEvent, useContext } from 'react';
import { Segment, Form, Button, Grid } from 'semantic-ui-react';
import {
    IActivity,
    IActivityFormValues,
    ActivityFormValues,
} from '../../../app/models/activity';
import { v4 as uuid } from 'uuid';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router-dom';
import { useEffect } from 'react';
import { Form as FinalForm, Field } from 'react-final-form';
import TextInput from '../../../app/common/form/TextInput';
import TextAreaInput from '../../../app/common/form/TextAreaInput';
import SelectInput from '../../../app/common/form/SelectInput';
import { category } from '../../../app/common/options/categoryOptions';
import DateInput from '../../../app/common/form/DateInput';
import { combineDateAndTime } from '../../../app/common/util/util';
import { RootStoreContext } from '../../../app/stores/rootStore';

interface DetailParams {
    id: string;
}

const ActivityForm: React.FC<RouteComponentProps<DetailParams>> = ({
    match,
    history,
}) => {
    const rootStore = useContext(RootStoreContext);
    const {
        createActivity,
        editActivity,
        submitting,
        activity: initialFormState,
        loadActivity,
        clearActivity,
    } = rootStore.activityStore;

    const [activity, setActivity] = useState(new ActivityFormValues());
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (match.params.id) {
            setLoading(true);
            loadActivity(match.params.id)
                .then((activity) =>
                    setActivity(new ActivityFormValues(activity))
                )
                .finally(() => setLoading(false));
        }
    }, [loadActivity, activity.id]);

    const handleFinalFormSubmit = (values: any) => {
        const dateAndTime = combineDateAndTime(values.date, values.time);
        const { date, time, ...activity } = values;
        // all properties of values without date and time are in activity now
        console.log(activity);
        activity.date = dateAndTime;
        console.log(activity);
    };

    // const handleSubmit = () => {
    //     if (activity.id.length === 0) {
    //         let newActivity = {
    //             ...activity,
    //             id: uuid(),
    //         };
    //         createActivity(newActivity).then(() =>
    //             history.push(`/activities/${newActivity.id}`)
    //         );
    //     } else {
    //         editActivity(activity).then(() =>
    //             history.push(`/activities/${activity.id}`)
    //         );
    //     }
    // };

    const handleInputChange = (
        event: FormEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { name, value } = event.currentTarget;
        setActivity({ ...activity, [name]: value });
    };

    return (
        <Grid>
            <Grid.Column width={10}>
                <Segment clearing>
                    <FinalForm
                        initialValues={activity}
                        onSubmit={handleFinalFormSubmit}
                        render={({ handleSubmit }) => (
                            <Form onSubmit={handleSubmit} loading={loading}>
                                <Field
                                    name='title'
                                    placeholder='Title'
                                    value={activity.title}
                                    component={TextInput}
                                />
                                <Field
                                    name='description'
                                    placeholder='Description'
                                    rows={3}
                                    value={activity.description}
                                    component={TextAreaInput}
                                />
                                <Field
                                    name='category'
                                    placeholder='Category'
                                    value={activity.category}
                                    component={SelectInput}
                                    options={category}
                                />
                                <Form.Group widths='equal'>
                                    <Field
                                        name='date'
                                        type='datetime-local'
                                        date={true}
                                        placeholder='Date'
                                        value={activity.date}
                                        component={DateInput}
                                    />
                                    <Field
                                        name='time'
                                        type='datetime-local'
                                        time={true}
                                        placeholder='Time'
                                        value={activity.time}
                                        component={DateInput}
                                    />
                                </Form.Group>

                                <Field
                                    name='city'
                                    placeholder='City'
                                    value={activity.city}
                                    component={TextInput}
                                />
                                <Field
                                    name='venue'
                                    placeholder='Venue'
                                    value={activity.venue}
                                    component={TextInput}
                                />
                                <Button
                                    loading={submitting}
                                    onClick={handleSubmit}
                                    floated='right'
                                    disabled={loading}
                                    positive
                                    type='submit'
                                    content='Submit'
                                />
                                <Button
                                    onClick={() => history.push('/activities')}
                                    floated='right'
                                    disabled={loading}
                                    type='submit'
                                    content='Cancel'
                                />
                            </Form>
                        )}
                    />
                </Segment>
            </Grid.Column>
        </Grid>
    );
};

export default observer(ActivityForm);
